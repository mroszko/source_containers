#!/bin/bash

# Find which OS is used
# This gives us two variables $ID which contains a string naming the OS
# and $VERSION_ID which contains a string with the OS version
# e.g. for Ubuntu 18.04:
#   $ID=ubuntu
#   $VERSION_ID=18.04
source /etc/os-release


########################################################################
# Parse the arguments
########################################################################
case $1 in
-h|--help)
    echo "KiCad dependency installer"
    echo ""
    echo "This script will install the dependencies needed to build"
    echo "KiCad from source. The dependencies installed can be configured"
    echo "by using the following flags to enable/disable installation:"
    echo "--use-wxpython:  Install the wxPython library"
    echo "--use-wxphoenix: Install wxPhoenix library"
    echo "--use-python2:   Install the Python 2 interpreter"
    echo "--use-python3:   Install the Python 3 interpreter"
    echo "--use-ngspice:   Install the ngspice library"
    echo "--use-oce:       Install OpenCascade Community Edition"
    echo "--use-occ:       Install OpenCascade"
    echo "--install-xvfb:  Install xvfb for running the unit tests headless"
    echo ""
    echo "For example, to install OpenCascade and not OCE:"
    echo "    $0 --use-occ=1 --use-oce=0"
    echo ""
    echo "Calling with no flags is the equivalent of providing:"
    echo "  --use-wxphoenix=1 --use-python3=1 --use-ngspice=1 --use-occ=1"
    exit
esac


########################################################################
# Test if 'sudo' is available, work around otherwise
#
# Ubuntu docker images run as root and do not have the sudo command available
########################################################################
if ! command -v sudo >/dev/null 2>&1;
then
    # sudo is not available
    if [[ "$EUID" == "0" ]];
    then
        # We are running as root, so use a dummy function that makes 'sudo command' just call 'command'
        function sudo()
        {
            $@
        }
    else
        echo "Sudo is not available, please run this script as root." >&2
        exit 1
    fi
fi


########################################################################
# Install Fedora dependencies
########################################################################
if [[ $ID == "fedora" ]];
then
    echo "Detected Fedora $VERSION_ID operating system"
    echo ""

    # All the magic happens in this script
    ./dependencyScripts/fedora.sh $@

########################################################################
# Install Ubuntu dependencies
########################################################################
elif [[ $ID == "ubuntu" ]];
then
    echo "Detected Ubuntu $VERSION_ID operating system"
    echo ""

    # All the magic happens in this script
    ./dependencyScripts/ubuntu.sh $@

########################################################################
# End of the if statement
########################################################################
fi
