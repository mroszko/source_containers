#!/bin/bash

source /etc/os-release

########################################################################
# Parse the arguments
########################################################################
case $1 in
-h|--help)
    echo "KiCad dependency installer for Fedora"
    echo ""
    echo "See the help in install-dependencies.sh for more information."
    exit
esac

USE_PYTHON2=0
USE_PYTHON3=1
USE_WXPYTHON=0
USE_WXPHOENIX=1
USE_NGSPICE=1
USE_OCC=1
USE_OCE=0
INSTALL_XVFB=0


while (( "$#" )); do
    case "$1" in
    --use-wxpython=*)
        USE_WXPYTHON="${1#*=}"
        if [[ $USE_PYTHON == 0 ]];
        then
            USE_PYTHON="${1#*=}"
        fi
        shift
        ;;
    --use-wxphoenix=*)
        USE_WXPHOENIX="${1#*=}"
        if [[ $USE_PYTHON == 0 ]];
        then
            USE_PYTHON="${1#*=}"
        fi
        shift
        ;;
    --use-python2=*)
        USE_PYTHON2="${1#*=}"
        shift
        ;;
    --use-python3=*)
        USE_PYTHON3="${1#*=}"
        shift
        ;;
    --use-ngspice=*)
        USE_NGSPICE="${1#*=}"
        shift
        ;;
    --use-oce=*)
        USE_OCE="${1#*=}"
        shift
        ;;
    --use-occ=*)
        USE_OCC="${1#*=}"
        shift
        ;;
    --install-xvfb=*)
        INSTALL_XVFB="${1#*=}"
        shift
        ;;
    --*) # unsupported flags
      echo "Error: Unknown flag $1" >&2
      exit 1
      break
      ;;
  esac
done


########################################################################
# Verify the options
########################################################################

# Print out the boolean logic
to_bool[0]="no"
to_bool[1]="yes"

echo "Configured options:"
echo "    Install Python 2: ${to_bool[$USE_PYTHON2]}"
echo "    Install Python 3: ${to_bool[$USE_PYTHON3]}"
echo "    Install wxPython: ${to_bool[$USE_WXPYTHON]}"
echo "    Install wxPhoenix: ${to_bool[$USE_WXPHOENIX]}"
echo "    Install ngspice: ${to_bool[$USE_NGSPICE]}"
echo "    Install OCE: ${to_bool[$USE_OCE]}"
echo "    Install OCC (OpenCascade): ${to_bool[$USE_OCC]}"
echo "    Install xvfb (X Virtual Framebuffer): ${to_bool[$INSTALL_XVFB]}"

echo ""


########################################################################
# Test if 'sudo' is available, work around otherwise
#
# Ubuntu docker images run as root and do not have the sudo command available
########################################################################
if ! command -v sudo >/dev/null 2>&1;
then
    # sudo is not available
    if [[ "$EUID" == "0" ]];
    then
        # We are running as root, so use a dummy function that makes 'sudo command' just call 'command'
        function sudo()
        {
            $@
        }
    else
        echo "Sudo is not available, please run this script as root." >&2
        exit 1
    fi
fi

########################################################################
# Install the packages
########################################################################

# Install development tools
PACKAGES="@development-tools \
    findutils \
    coreutils \
    grep \
    git \
    cmake \
    lemon \
    make \
    ninja-build \
    ccache \
    automake \
    gcc-c++ \
    gdb \
    clang \
    clang-libs \
    clang-tools-extra \
    git-clang-format \
    libasan \
    doxygen \
    graphviz"

# These are the core dependencies
PACKAGES="$PACKAGES \
    mesa-libGLw-devel \
    glew-devel \
    glm-devel \
    libcurl-devel \
    cairo-devel \
    boost-devel \
    openssl-devel \
    unixODBC-devel \
    gtk3-devel"
    
if [[ $VERSION_ID == "36" ]];
then
PACKAGES="$PACKAGES \
    wxGTK3-devel"
else
PACKAGES="$PACKAGES \
    wxGTK-devel"
fi

# Install the x virtual framebuffer if requested
if [ $INSTALL_XVFB == 1 ];
then
    PACKAGES="$PACKAGES \
        xorg-x11-server-Xvfb"
fi

# Install Python 2 and the appropriate wxPython packages
if [ $USE_PYTHON2 == 1 ];
then
    # If using Python, we need SWIG to compile the interface
    PACKAGES="$PACKAGES \
        swig \
        python2-devel"

    # Plain old wxPython
    if [ $USE_WXPYTHON == 1 ];
    then
        PACKAGES="$PACKAGES wxPython-devel"
    fi

    # The new wxPhoenix
    if [ $USE_WXPHOENIX == 1 ];
    then
        PACKAGES="$PACKAGES python2-wxpython4"
    fi
fi

# Install Python 3 and the appropriate wxPython packages
if [ $USE_PYTHON3 == 1 ];
then
    # If using Python, we need SWIG to compile the interface
    PACKAGES="$PACKAGES \
        swig \
        python3-devel"

    # Plain old wxPython doesn't exist in Fedora for Python 3
    if [ $USE_WXPYTHON == 1 ];
    then
        echo "Error: wxPython is not available for Python 3" >&2
        exit 1
    fi

    # The new wxPhoenix
    if [ $USE_WXPHOENIX == 1 ];
    then
        PACKAGES="$PACKAGES python3-wxpython4"
    fi
fi

# Fedora's packaged ngspice shared library
if [ $USE_NGSPICE == 1 ];
then
    PACKAGES="$PACKAGES \
        ngspice \
        libngspice \
        libngspice-devel"
fi

# OpenCascade
if [ $USE_OCC == 1 ];
then
    if [ $USE_OCE == 1 ];
    then
        echo "Error: Fedora cannot have both OCE and OCC installed" >&2
        exit 1
    fi

    PACKAGES="$PACKAGES opencascade-devel"

fi

# OCE has been retired on Fedora in favor of opencascade
if [ $USE_OCE == 1 ];
then
    echo "OCE is retired on Fedora"
    PACKAGES="$PACKAGES OCE-devel"
fi

echo "Installing packages: " $PACKAGES
echo ""

sudo dnf -y install $PACKAGES
