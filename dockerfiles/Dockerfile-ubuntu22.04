FROM ubuntu:22.04

# The Ubuntu docker image runs as root, and also doesn't even provide
# the sudo command, so we don't ever need to use it here

# The tzdata package will drop to a user prompt for questions
# so we have to tell dpkg that we don't want interaction
ARG DEBIAN_FRONTEND=noninteractive

# Ubuntu doesn't package the add-apt-repository command inside the image
# so we must get it ourselves so we can install PPAs easily
RUN apt-get update && \
    apt-get install -y -qq apt-utils software-properties-common

# Pull in the dependency script
COPY scripts/dependencyScripts/ubuntu.sh /

# Actually install our dependencies
RUN chmod +x /ubuntu.sh && /ubuntu.sh --install-xvfb=1 --ext-doxygen=1

# Newer doxygen has to be built manually for now
COPY scripts/dependencyScripts/doxygen-build.sh /

RUN chmod +x /doxygen-build.sh && /doxygen-build.sh

# Cleanup afterwards to make the image smaller
RUN apt-get remove -y software-properties-common apt-utils && \
    apt-get clean && \
    rm -rf /ubuntu.sh && \
    rm -rf /doxygen-build.sh && \
    rm -rf /var/lib/apt/lists/*

# Include a test script in the image so we can make sure the libraries are installed
COPY scripts/test-install.sh /
